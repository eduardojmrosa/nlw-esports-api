#Backend

##Entities

###Game
id
title
bannerUrl

###Ads

id
gameId
name
yearsPlaying
discord
weekDays
hourStart
hourEnd
useVoiceChannel
createdAt

##Use Case

-Listagem de games
-Criação de um novo anúncio
-Listagem de anúncio por game
-Buscar discord pelo id do anúncio
